package com.example.primeraapp4a;

import android.support.v7.app.AppCompatActivity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.*;
import android.widget.Button;


public class Fragmentos extends AppCompatActivity implements View.OnClickListener,  FrgUno.OnFragmentInteractionListener, FrgDos.OnFragmentInteractionListener {


    Button Fragmentouno, Fragmentodos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentos);
        Fragmentouno= (Button) findViewById(R.id.btnuno);
        Fragmentodos= (Button) findViewById(R.id.btndos);

        Fragmentouno.setOnClickListener(this);
        Fragmentodos.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnuno:
                FrgUno fragmentoUno = new FrgUno();
                //getActivity().getSupportFragmentManager().beginTransaction();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,fragmentoUno);
                transaction.commit();
                break;
        }
        switch (v.getId()){
            case R.id.btndos:
                FrgDos fragmentoDos = new FrgDos();
                //getActivity().getSupportFragmentManager().beginTransaction();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,fragmentoDos);
                transaction.commit();
                break;

        }
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

}


