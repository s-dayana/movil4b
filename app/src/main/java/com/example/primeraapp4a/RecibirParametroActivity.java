package com.example.primeraapp4a;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class RecibirParametroActivity extends AppCompatActivity {
    TextView txtresultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibir_parametro);

        txtresultado = (TextView)findViewById(R.id.txtresultado);
        Bundle bundle = this.getIntent().getExtras();
        txtresultado.setText(bundle.getString("dato"));
    }


}
