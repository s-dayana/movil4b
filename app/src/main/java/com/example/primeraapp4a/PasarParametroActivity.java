package com.example.primeraapp4a;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;

public class PasarParametroActivity extends AppCompatActivity {

    EditText cajaDatos;
    Button botonEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasar_parametro);

        cajaDatos = (EditText) findViewById(R.id.txtenviar);
        botonEnviar = (Button) findViewById(R.id.btnenviar);

        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PasarParametroActivity.this, RecibirParametroActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato", cajaDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
